package com.company;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Actor {
    protected int x,y;
    protected int height, width;
    protected String spriteName;
    protected Stage stage;
    protected SpriteCache spriteCache;


    public Actor(Stage stage){
        this.stage = stage;
        spriteCache = stage.getSpriteCache();
    }

    public void paint(Graphics2D g){
        g.drawImage( spriteCache.getSprite(spriteName), x, y, stage );
    }

    public int getX(){ return x; }
    public void setX(int i){ this.x = i; }

    public int getY(){ return y; }
    public void setY(int i){ this.y = i; }

    public String getSpriteName(){ return spriteName; }

    public void setSpriteName(String string){
        spriteName = string;
        BufferedImage image = spriteCache.getSprite(spriteName);
        height = image.getHeight();
        width = image.getWidth();
    }

    public int getHeight(){ return height; }
    public void setHeight(int i){ this.height = i; }

    public int getWidth(){ return width; }
    public void setWidth(int i){ this.width = i; }

    public void act(){

    }
}

