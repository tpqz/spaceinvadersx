package com.company;
import java.nio.Buffer;

import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.awt.image.BufferStrategy;
import java.util.ArrayList;

public class SpaceInvadersX extends Canvas implements Stage {
    public long usedTime;
    public BufferStrategy buffer;
    public SpriteCache spriteCache;
    private ArrayList actors;

        public SpaceInvadersX(){

            spriteCache = new SpriteCache();
            JFrame frame = new JFrame();
            JPanel panel = (JPanel)frame.getContentPane();
            setBounds(0, 0, Stage.WIDTH, Stage.HEIGHT);
            panel.setPreferredSize(new Dimension(Stage.WIDTH, Stage.HEIGHT));
            panel.setLayout(null);
            panel.add(this);

            frame.setTitle("SpaceInvadersX");
            frame.setBounds(50, 50, Stage.WIDTH, Stage.HEIGHT);
            frame.setVisible(true);
            frame.setResizable(false);
            frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

            createBufferStrategy(2);
            buffer = getBufferStrategy();
            requestFocus();
        }

        public void initWorld() {
            actors = new ArrayList();
            for (int i=0; i<10; i++){
                Monster m = new Monster(this);
                m.setX((int)(Math.random()*Stage.WIDTH));
                m.setY( i * 20);
                m.setVx((int) (Math.random()*3)+1);
                actors.add(m);
            }
        }

        public void paintWorld(){ //image of invader
            Graphics2D g = (Graphics2D)buffer.getDrawGraphics();
            g.setColor(Color.black);
            g.fillRect(0, 0, getWidth(), getHeight());
            for (int i= 0; i < actors.size(); i++){
                Actor m = (Actor)actors.get(i);
                m.paint(g);
            }
            g.setColor(Color.WHITE);
            if (usedTime > 0)
                g.drawString(String.valueOf(1000/usedTime)+ " fps", 5, Stage.HEIGHT - 50);
            else
                g.drawString("--- fps",5,Stage.HEIGHT - 50);
            buffer.show();
        }

        public void updateWorld(){
           for (int i=0; i < actors.size(); i++){
               Actor m = (Actor)actors.get(i);
               m.act();
           }
        }

        public SpriteCache getSpriteCache(){
            return spriteCache;
        }

        public void game(){
            usedTime = 1000;
            initWorld();
            while (isVisible()) {
                long startTime = System.currentTimeMillis();
                updateWorld();
                paintWorld();
                usedTime = System.currentTimeMillis()-startTime;
                try{
                    Thread.sleep(Stage.SPEED);
                }catch (InterruptedException e){
                }
            }
        }

    public static void main(String[] args) {
        SpaceInvadersX window = new SpaceInvadersX();
            window.game();

    }
}
