package com.company;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.HashMap;



public class SpriteCache {
    public HashMap sprites;

    public SpriteCache() {
        sprites = new HashMap();
    }


    public BufferedImage loadImage(String path){ //method that loads a image
        URL url = null;

        try{
            url = getClass().getClassLoader().getResource(path);
            return ImageIO.read(url);
        } catch (Exception e){
            System.out.println("Przy otwieraniu: " + path + " jako " + url);
            System.out.println("Wystapil blad: " + e.getClass().getName() +" " + e.getMessage());
            System.exit(0);
            return null;
        }
    }

    public BufferedImage getSprite(String path){
        BufferedImage img = (BufferedImage)sprites.get(path);
        if (img == null){
            img = loadImage("images/" + path);
            sprites.put(path,img);
        }
        return img;
    }
}
