package com.company;
import java.awt.image.ImageObserver;
/**
 * Created by Mateusz on 12.03.2016.
 */
public interface Stage extends ImageObserver{
    public static final int HEIGHT = 600;
    public static final int WIDTH = 800;
    public static final int SPEED = 10;
    public SpriteCache getSpriteCache();
}